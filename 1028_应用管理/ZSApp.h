//
//  ZSApp.h
//  1028_应用管理
//
//  Created by @Zs on 15/10/29.
//  Copyright © 2015年 @Zs. All rights reserved.
//  模型类

#import <Foundation/Foundation.h>
/**
 *  copy:NSString
    strong:一般对象
    weak:UI控件
    assign:基本数据类型
 */
@interface ZSApp : NSObject
/**
 *  应用的名称
 */
@property(nonatomic,copy)NSString *name;
/**
 *  应用的图标
 */
@property(nonatomic,copy)NSString *icon;
/**
 *  通过字典初始化模型
 *
 *  @param dict 字典对象
 *
 *  @return 已经初始化完毕的模型对象
 */
-(instancetype)initWithDict:(NSDictionary *)dict;

+(instancetype)appWithDict:(NSDictionary *)dict;

@end
