//
//  ZSApp.m
//  1028_应用管理
//
//  Created by @Zs on 15/10/29.
//  Copyright © 2015年 @Zs. All rights reserved.
//  模型类

#import "ZSApp.h"

@implementation ZSApp

-(instancetype)initWithDict:(NSDictionary *)dict{

    if (self=[super init]) {
    
        self.name = dict[@"name"];
        self.icon = dict[@"icon"];
    }
    return self;
}

+(instancetype)appWithDict:(NSDictionary *)dict{

    //self alloc 保证任何进来调用的 想要得到得返回结果都是正确的
    //直接携程ZSApp 不论谁调用返回结果都是ZSApp  这种情况下,在将来出现继承此类的子类的情况下就变得不对了
    ZSApp *app = [[self alloc]initWithDict:dict];
    
    return app;
    
}

@end
