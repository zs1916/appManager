//
//  ZSAppView.m
//  1028_应用管理
//
//  Created by @Zs on 15/10/29.
//  Copyright © 2015年 @Zs. All rights reserved.
//

#import "ZSAppView.h"
#import "ZSApp.h"

@interface ZSAppView()

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@end
@implementation ZSAppView

-(void)setApp:(ZSApp *)app{


    _app = app;

    //1.设置图标
    self.iconView.image = [UIImage imageNamed:app.icon];
    //2.设置名字
    self.nameLable.text = app.name; 
}

+(instancetype)appView{

    //3.1 创建view
    NSBundle *bundle = [NSBundle mainBundle];
    //3.2 读取xib文件(会创建xib中描述的所有对象,并且按顺序放到数组中返回)
    NSArray *objs = [bundle loadNibNamed:@"ZSAppView" owner:nil options:nil];
    return  objs.lastObject;

}

@end
