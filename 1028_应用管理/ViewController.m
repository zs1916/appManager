//
//  ViewController.m
//  1028_应用管理
//
//  Created by @Zs on 15/10/28.
//  Copyright © 2015年 @Zs. All rights reserved.
//

#import "ViewController.h"
#import "ZSApp.h"
#import "ZSAppView.h"

@interface ViewController ()
/**存放应用信息*/
@property(nonatomic,strong)NSArray *apps;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //0.总列数
    int totalColumns = 3;
    
    
    
    //1.应用的尺寸
    CGFloat appW = 100;
    CGFloat appH = 130;
    //2.间隙
    CGFloat marginX = (self.view.frame.size.width - totalColumns*appW)/(totalColumns+1);//水平间距
    CGFloat marginY = 40;//垂直间距
    //3.根据应用的plist的长度 决定创建的个数
    
    for (int index = 0; index<self.apps.count; index++) {
        
        
        
        
//        UIViewController *appViewController = objs.firstObject;
//    #warning
//        /**
//         unrecognized selector sent to instance
//         *  这里出过错误,在xib里把UIView拖成了UIViewController,导致在实例化的时报错
//         
//            UIViewController和UIview的区别是UIViewController返回的是一个控制器,UIView只是一个view
//         */
//        UIView *appView =  appViewController.view;
        
        //3.1 创建view
        ZSAppView *appView = [ZSAppView appView];
        
        //3.3 添加view
        [self.view addSubview:appView];
        
        //3.4 设置数据
        int row = index/totalColumns;
        int col = index%totalColumns;
        // 设置frame
        CGFloat appX = marginX + col *(appW +marginX);
        CGFloat appY = 50 + row * (appH + marginY);
        appView.frame = CGRectMake(appX, appY, appW, appH);

        ZSApp *app = self.apps[index];
        //设置数据
        appView.app = app;
        
        //3.4.1 设置图片
        
//        UIImageView *iconView = appView.subviews[0];
//        iconView.image = [UIImage imageNamed:app.icon];
        
        //3.4.2 设置名字

//        UILabel *nameLable = (UILabel *)[appView viewWithTag:20];
//        nameLable.text = app.name;
        
    }
    
    
    
    
    
}

-(NSArray *)apps{

    if (_apps == nil) {
        //初始化加载数据
        //1.获得plist的全路径
        NSString *path = [[NSBundle mainBundle]pathForResource:@"app.plist" ofType:nil];
        
        //2.加载数组
        NSArray *dictArray = [NSArray arrayWithContentsOfFile:path];
        
        //3.将数组中所有的字典 转换成模型对象,放到新的数组中
        NSMutableArray *appArray = [NSMutableArray array];
        for (NSDictionary *dict in dictArray) {
            //3.1创建模型对象
            ZSApp *app = [ZSApp appWithDict:dict];
            //3.2将字典的所有属性赋值给模型
            [appArray addObject:app];
        }
        //4.赋值
        _apps = appArray;
        
        
    }
    
    return _apps;

}



@end
