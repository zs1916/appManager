//
//  ZSAppView.h
//  1028_应用管理
//
//  Created by @Zs on 15/10/29.
//  Copyright © 2015年 @Zs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSApp;

@interface ZSAppView : UIView
/**
 *  模型数据
 */
@property(nonatomic,strong)ZSApp *app;

+(instancetype)appView;
@end
